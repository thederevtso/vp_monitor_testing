﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Data.SQLite;


namespace TestPreps
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("TestPreps. For exit type \"exit\"");

            //string continueString = "";

            //while (!(String.Equals(continueString, "exit")))
            //{
            //    Console.WriteLine("Default prep string: {0}", 
            //        Properties.Settings.Default.DefString);

            //    Properties.Settings.Default.Reload();
            //    continueString = Console.ReadLine();
            //}

            TestPostgreSQL();
            Console.WriteLine();
            //TestSQLite();

            Console.WriteLine();

            TestHTTPWebRequest();


            Console.WriteLine("Нажмите Enter...");
            Console.ReadLine();


        }

        private static void TestHTTPWebRequest()
        {
            throw new NotImplementedException();
        }

        private delegate TimeSpan FakeRetrieveDataHandler(string databaseName);

        private static void TestSQLite()
        {
            ColorMessage("Test SQLite");

            string databaseName1 = @"DB\cyber1.db";
            string databaseName2 = @"C:\dikey\public\cyber2.db";
            string databaseName3 = @"DB\cyber3.db";

            //CreateSQLiteDB(databaseName);

            //CreateSQLiteTable(databaseName);

            //InsertSQLiteStrings(databaseName);


            FakeRetrieveDataHandler ret1 = RetreiveSQLiteData;
            FakeRetrieveDataHandler ret2 = RetreiveSQLiteData;
            FakeRetrieveDataHandler ret3 = RetreiveSQLiteData;

            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();


            

            sw.Start();
            IAsyncResult res1 = ret1.BeginInvoke(databaseName1, null, null); 
            IAsyncResult res2 = ret2.BeginInvoke(databaseName2, null, null); 
            IAsyncResult res3 = ret3.BeginInvoke(databaseName3, null, null);
            

            TimeSpan ts1 = ret1.EndInvoke(res1);
            TimeSpan ts2 = ret2.EndInvoke(res2);
            TimeSpan ts3 = ret3.EndInvoke(res3);

            sw.Stop();

            Console.WriteLine("Параллельно: ");
            Console.WriteLine("ts1: {0}", ts1);
            Console.WriteLine("ts2: {0}", ts2);
            Console.WriteLine("ts3: {0}", ts3);
            Console.WriteLine("Сумма: {0}", ts1 + ts2 + ts3);
            Console.WriteLine("Среднее: {0}", new TimeSpan((ts1 + ts2 + ts3).Ticks/3));

            TimeSpan tsAll = sw.Elapsed;
            Console.WriteLine("На все операции ушло: {0}", tsAll.ToString());
            sw.Reset();


            Console.WriteLine();

            sw.Start();
            Console.WriteLine("Последовательно: ");
            ts1 = RetreiveSQLiteData(databaseName1);
            ts2 = RetreiveSQLiteData(databaseName2);
            ts3 = RetreiveSQLiteData(databaseName3);
            sw.Stop();

            Console.WriteLine("ts1: {0}", ts1);
            Console.WriteLine("ts2: {0}", ts2);
            Console.WriteLine("ts3: {0}", ts3);
            Console.WriteLine("Сумма: {0}", ts1 + ts2 + ts3);
            Console.WriteLine("Среднее: {0}", new TimeSpan((ts1 + ts2 + ts3).Ticks / 3));

            tsAll = sw.Elapsed;
            Console.WriteLine("На все операции ушло: {0}", tsAll);
        }

        private static TimeSpan RetreiveSQLiteData(string databaseName)
        {
            using (SQLiteConnection connection =
                new SQLiteConnection(string.Format("Data Source={0}", databaseName)))
            {
                connection.Open();
                SQLiteCommand command = new SQLiteCommand("SELECT * FROM 'example';", connection);
                SQLiteDataReader reader = command.ExecuteReader();

                Console.Write("\u250C" + new string('\u2500', 5) + "\u252C" + new string('\u2500', 60) + "\u2510");
                Console.WriteLine("\n\u2502" + "  id \u2502" + new string(' ', 30) + "value" + new string(' ', 25) + "\u2502");
                Console.Write("\u251C" + new string('\u2500', 5) + "\u253C" + new string('\u2500', 60) + "\u2524\n");
                //foreach (DbDataReader record in reader)
                //{
                //    string id = record["id"].ToString();
                //    id = id.PadLeft(5 - id.Length, ' ');

                //    string value = record["value"].ToString();

                //    string result = "\u2502" + id + "\u2502";

                //    value = value.PadLeft(60, ' ');

                //    result += value + "\u2502";
                //    Console.WriteLine(result);
                //}
                string id;
                string value;
                string result;

                System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
                stopwatch.Start();
                while (reader.Read())
                {
                    //StringBuilder sb = new StringBuilder(reader["id"].ToString());
                    id = reader["id"].ToString();
                    //id = reader.GetInt32(0).ToString();

                    //id = id.PadLeft(6 - id.Length, ' ');

                    //value = reader["value"].ToString();
                    value = reader.GetString(1);

                    result = "\u2502" + id + " \u2502";

                    //value = value.PadLeft(60, ' ');

                    result += value + "\u2502";
                    //Console.WriteLine(result);
                }
                stopwatch.Stop();
                TimeSpan ts = stopwatch.Elapsed;


                Console.WriteLine("\u2514" + new string('\u2500', 6) + "\u2534" + new string('\u2500', 60) + "\u2518");
                connection.Close();

                return ts;
            }
        }

        private static void InsertSQLiteStrings(string databaseName)
        {
            ColorMessage("Вставляем данные в таблицу");
            using (SQLiteConnection connection =
                new SQLiteConnection(string.Format("Data Source={0}", databaseName)))
            {
                connection.Open();
                //SQLiteCommand command = new SQLiteCommand(@"INSERT INTO 'example' ('id', 'value') VALUES 
                //    (1, 'Вася'), (2, 'Petya'), (3,'NotPetya');"
                //    , connection);

                using (SQLiteTransaction tr = connection.BeginTransaction())
                {

                    SQLiteCommand command = new SQLiteCommand(@"INSERT INTO 'example' ('value') VALUES 
                    ('Вася'), ('Petya'), ('NotPetya');"
                        , connection);

                    for (int i = 0; i < 150000; i++)
                    {
                        command.ExecuteNonQuery();
                    }

                    tr.Commit();
                }



                connection.Close();
                Console.WriteLine("Данные вставлены");
            }

        }

        private static void CreateSQLiteTable(string databaseName)
        {
            ColorMessage("Создаём таблицу");
            using (SQLiteConnection connection =
                new SQLiteConnection(string.Format("Data Source = {0};", databaseName)))
            {
                SQLiteCommand command =
                    new SQLiteCommand("CREATE TABLE example (id INTEGER PRIMARY KEY AUTOINCREMENT, value TEXT);", connection);

                connection.Open();
                command.ExecuteNonQuery();
                connection.Close();

                Console.WriteLine("Таблица создана");
            }



        }

        private static void CreateSQLiteDB(string databaseName)
        {
            ColorMessage("CreateSQLiteDB");

            if (!Directory.Exists(@"DB"))
            {
                Directory.CreateDirectory(@"DB");
            }
            SQLiteConnection.CreateFile(databaseName);
            Console.WriteLine(File.Exists(databaseName) ? "БД Создана" : "Возникла ошибка при создании базы данных");
        }

        private static void TestPostgreSQL()
        {
            ColorMessage("Test PostgresQL");

            string connectionString = "Host=10.2.4.23;Port=5432;Database=VoicePortal;Username=postgres;Password=postgres01";

            NpgsqlConnectionStringBuilder conStrB = new NpgsqlConnectionStringBuilder(connectionString);

            using (NpgsqlConnection conn = new NpgsqlConnection(conStrB))
            {
                conn.Open();

                using (NpgsqlCommand cmd = new NpgsqlCommand("SELECT mpp FROM public.vpmppstate;", conn))
                {
                    using (NpgsqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Console.WriteLine(reader.GetString(0));
                        }
                    }
                }

            }
        }


        /// <summary>
        /// Отправить в консоль цветное сообщение через <see cref="Console.WriteLine()"/>
        /// </summary>
        /// <param name="message">сообщение</param>
        /// <param name="args">аргументы <see cref="Console.WriteLine()"/> по необходимости</param>
        private static void ColorMessage(object message, params object[] args)
        {
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine(message.ToString(), args);
            Console.ResetColor();
        }
    }
}
