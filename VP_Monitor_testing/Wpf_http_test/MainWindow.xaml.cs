﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Wpf_http_test
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void request_Click(object sender, RoutedEventArgs e)
        {
            Uri uriRes;


            //HACK: cert is dead
            ServicePointManager.ServerCertificateValidationCallback += (_sender, certificate, chain, sslPolicyErrors) => true;



            try
            {
                 uriRes = new Uri(txb_url.Text);
            }
            catch (Exception err)
            {

                txb_serverInfo.Text = err.ToString();
                return;
            }
            
            // Создать объект запроса
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uriRes);

            request.Timeout = 20000;

            HttpWebResponse response;
            // Получить ответ с сервера
            try
            {
                txb_sourceCode.Text = "";

                if (checkBoxUsingProxy.IsChecked == true)
                {
                    
                    request.Proxy.Credentials = CredentialCache.DefaultNetworkCredentials;
                }
                else
                {
                    request.Proxy = null;
                }

                response = (HttpWebResponse)request.GetResponse();

                StringBuilder textOf = new StringBuilder();

                //// Получаем поток данных из ответа
                //using (StreamReader stream = new StreamReader(response.GetResponseStream()))
                //{
                //    // Выводим исходный код страницы
                //    string line;
                //    while ((line = stream.ReadLine()) != null)
                //        txb_sourceCode.Text += line + "\n";
                //    textOf.Append(line);
                //}

                using (StreamReader stream = new StreamReader(response.GetResponseStream()))
                {
                    textOf.Append(stream.ReadToEnd());
                }


                // Получаем некоторые данные о сервере
                string messageServer = "Целевой URL: \t" + request.RequestUri + "\nМетод запроса: \t" + request.Method +
                     "\nТип полученных данных: \t" + response.ContentType + "\nДлина ответа: \t" + response.ContentLength + "\nЗаголовки";

                // Получаем заголовки, используем LINQ
                WebHeaderCollection whc = response.Headers;
                var headers = Enumerable.Range(0, whc.Count)
                                        .Select(p =>
                                        {
                                            return new
                                            {
                                                Key = whc.GetKey(p),
                                                Names = whc.GetValues(p)
                                            };
                                        });

                foreach (var item in headers)
                {
                    messageServer += "\n  " + item.Key + ":";
                    foreach (var n in item.Names)
                        messageServer += "\t" + n;
                }

                txb_serverInfo.Text = messageServer;


                webBrowser.NavigateToString(textOf.ToString());


            }
            catch (WebException webErr)
            {

                txb_serverInfo.Text = webErr.Status.ToString() + ' ' + webErr.Message + "\n";

                if (webErr.Status == WebExceptionStatus.ProtocolError && webErr.Response != null)
                {                  
                    HttpWebResponse httpErrResp = webErr.Response as HttpWebResponse;
                    txb_serverInfo.Text += httpErrResp.StatusCode.GetHashCode().ToString() + ' ' + httpErrResp.StatusDescription;


                    string textOf = new StreamReader(httpErrResp.GetResponseStream()).ReadToEnd();
                    txb_sourceCode.Text = textOf;
                    webBrowser.NavigateToString(textOf);
                }

            }
        }

        private void txb_url_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                request_Click(this, null);
            }
        }
    }
}
