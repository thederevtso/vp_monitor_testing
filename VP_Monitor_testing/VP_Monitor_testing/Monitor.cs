﻿using System;
using System.Collections.Generic;

namespace VP_Monitor_testing
{
    internal class Monitor
    {
        public Monitor()
        {
            VoicePortal vp = new VoicePortal();
            StatusResponse status = vp.GetStatusResponse();
            if (!(status.TestIsPassed))
            {
                Console.WriteLine("ErrorList: ", String.Concat(ErrorList));
            } else
                Console.WriteLine("Test passed!");

        }

        public List<StatusResponse> ErrorList = new List<StatusResponse>();

        public class VoicePortal : VPSystem
        {
            public VoicePortal()
            {
                this.ListBranchedSystems.Add(new VPMS());
            }

            internal override StatusResponse GetLeafStatus()
            {
                Console.WriteLine("GetLeafStatus base"); //TODO: ChildSystems get status
                return new StatusResponse(true, "Response: GetLeafStatus base");
            }
        }

        public class VPMS : VPSystem
        {
            internal override StatusResponse GetLeafStatus()
            {
                Console.WriteLine("GetLeafStatus vpms"); //TODO: ChildSystems get status
                return new StatusResponse(true, "Response: GetLeafStatus vpms");
            }
        }

        public class StatusResponse
        {

            public bool TestIsPassed = true;
            public string ResponseText = "";


            public StatusResponse(bool TestIsPassed = true, string ResponseText = "")
            {
                this.TestIsPassed = TestIsPassed;
                this.ResponseText = ResponseText;
            }

            public override string ToString()
            {
                return ResponseText;
            }
        }



        public abstract class VPSystem
        {
            private const int IS_LEAF = 0;

            public StatusResponse GetStatusResponse()
            {
                StatusResponse response;
                if (ListBranchedSystems.Count == IS_LEAF)
                    return GetLeafStatus();
                else
                {
                    response = new StatusResponse();

                    foreach (VPSystem vpSystem in ListBranchedSystems)
                    {
                        if (!GetStatusResponse().TestIsPassed)
                        {
                            response.TestIsPassed = false;
                        }
                    }

                    return response;
                }

            }


            internal abstract StatusResponse GetLeafStatus();
            public VPSystem BaseSystem;
            public List<VPSystem> ListBranchedSystems = new List<VPSystem>();

            public string Title
            {
                get;
                private set;

            }

            public string Description
            {
                get;
                private set;
            }
        }

        
        
    }
}